module NewBowlingCalculator
[<Literal>]
let Strike = 'X'
[<Literal>]
let Spare = '/'
[<Literal>]
let Miss = '-'

type Frame =
    {
        ThrowOne : char
        ThrowOneScore : int
        ThrowTwo : char option
        ThrowTwoScore : int
    }
    
let splitLine (line : string) = line.Split(' ') |> Array.toList

let charToInt c = 
    if c = Miss then
        0
    else
        int c - int '0'

let createFrame (frame : string) = 
    if frame.Length > 1 then
        if frame.[1] = Spare then
            { ThrowOne = frame.[0];
             ThrowOneScore = frame.[0] |> charToInt;
              ThrowTwo = Some Spare;
               ThrowTwoScore = (10 - (frame.[0] |> charToInt)) }
        else
            { ThrowOne = frame.[0];
             ThrowOneScore = frame.[0] |> charToInt;
              ThrowTwo = Some frame.[1];
               ThrowTwoScore = (frame.[1] |> charToInt) }
    elif frame.Length > 0 && frame.[0] = Strike then
        { ThrowOne = frame.[0];
         ThrowOneScore = 10;
          ThrowTwo = None;
           ThrowTwoScore = 0 }
    else
        { ThrowOne = Miss; ThrowOneScore = 0; ThrowTwo = None; ThrowTwoScore = 0 }   
    
let calculateStrikeScore frameOne frameTwo = 
    let frameOneThrowTwo = match frameOne.ThrowTwo with
                           | Some n -> n
                           | None -> '-'
    let frameTwo = match frameTwo with
                   | Some n -> n
                   | None -> createFrame ""
    
    if frameOne.ThrowOne = Strike then
        frameOne.ThrowOneScore + frameTwo.ThrowOneScore
    else 
        frameOne.ThrowOneScore + frameOne.ThrowTwoScore

let calculateSpareScore frame =
    frame.ThrowOneScore
    
let calculateBonusScore fromStrike frame =
    if fromStrike then
        frame.ThrowOneScore + frame.ThrowTwoScore
    else
        frame.ThrowOneScore
    
let rec calculateTotalScore (frames : Frame list) score index =
    if index > 9 then
        score
    else
        let frame = frames.[index]
        let frameThrowTwo = match frame.ThrowTwo with
                                       | Some n -> n
                                       | None -> '-'
                                         
        if frame.ThrowOne = Strike then
            if index = 9 && frames.[index + 1].ThrowOne <> Strike then
                let bonusScore = calculateBonusScore true frames.[index + 1]
                let newScore = score + bonusScore + frame.ThrowOneScore + frame.ThrowTwoScore
                
                calculateTotalScore frames newScore (index + 1)
            else
                let strikeScore = calculateStrikeScore frames.[index + 1] (Some frames.[index + 2])
                let newScore = score + strikeScore + frame.ThrowOneScore + frame.ThrowTwoScore
            
                calculateTotalScore frames newScore (index + 1)
        elif frameThrowTwo = Spare then
            if index = 9 then
                let bonusScore = calculateBonusScore false frames.[index + 1]
                let newScore = score + bonusScore + frame.ThrowOneScore + frame.ThrowTwoScore
                
                calculateTotalScore frames newScore (index + 1)
            else
                let spareScore = calculateSpareScore frames.[index + 1]
                let newScore = score + spareScore + frame.ThrowOneScore + frame.ThrowTwoScore
                
                calculateTotalScore frames newScore (index + 1)
        else
            let newScore = score + frame.ThrowOneScore + frame.ThrowTwoScore
            
            calculateTotalScore frames newScore (index + 1)

let processLine line = 
    let frames = line |> splitLine |> List.map(createFrame)
    calculateTotalScore frames 0 0