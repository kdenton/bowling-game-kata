﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.
module Program

open System
open NewBowlingCalculator

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    
    printfn "Please enter the bowling game line: "
    let line = Console.ReadLine()
    
    printfn "Your score is: %i" (processLine line)
    0 // return an integer exit code
