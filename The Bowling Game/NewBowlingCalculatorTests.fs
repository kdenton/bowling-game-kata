module NewBowlingCalculatorTests

open NewBowlingCalculator
open Xunit

[<Fact>]
let ``createFrame takes a string and returns and Frame record type`` () =
    let expectedFrame = { ThrowOne = '5'; ThrowOneScore = 5; ThrowTwo = Some '5'; ThrowTwoScore = 5 }
    let frame = createFrame "55"
    Assert.Equal(expectedFrame, frame)
    
[<Fact>]
let ``createFrame will not provide optional ThrowTwo if a strike and return correct Frame`` () =
    let expectedFrame = { ThrowOne = 'X'; ThrowOneScore = 10; ThrowTwo = None; ThrowTwoScore = 0 }
    let frame = createFrame "X"
    Assert.Equal(expectedFrame, frame)
    
[<Fact>]
let ``createFrame will return a spare symbol for throw two and the correct scores`` () =
    let expectedFrame = { ThrowOne = '3'; ThrowOneScore = 3; ThrowTwo = Some '/'; ThrowTwoScore =  7 }
    let frame = createFrame "3/"
    Assert.Equal(expectedFrame, frame)
    
[<Fact>]
let ``createFrame if there is a miss in the score it is scored as a zero`` () =
    let expectedFrame = { ThrowOne = '3'; ThrowOneScore = 3; ThrowTwo = Some '-'; ThrowTwoScore = 0 }
    let frame = createFrame "3-"
    Assert.Equal(expectedFrame, frame)
    
[<Fact>]
let ``createFrame when given an empty string returns Frame with no score`` () =
    let expectedFrame = { ThrowOne = '-'; ThrowOneScore = 0; ThrowTwo = None; ThrowTwoScore = 0 }
    let frame = createFrame ""
    Assert.Equal(expectedFrame, frame)
    
[<Fact>]
let ``calculateStrikeScore when given a frame with a strike it returns the correct score`` () =
    let frameOne = createFrame "X"
    let frameTwo = createFrame "36"
    Assert.Equal(13, calculateStrikeScore frameOne (Some frameTwo))
    
[<Fact>]
let ``calculateStrikeScore when given a frame with a strike and the next frame has a strike it scores 20`` () =
    let frameOne = createFrame "X"
    let frameTwo = createFrame "X"
    Assert.Equal(20, calculateStrikeScore frameOne (Some frameTwo))
    
[<Fact>]
let ``calculateSpareScore when given a frame with a spare it returns the correct score`` () =
    let frame = createFrame "34"
    Assert.Equal(3, calculateSpareScore frame)
    
[<Fact>]
let ``calculateSpareScore when given a frame with a spare it and a strike in the next it scores 10`` () =
    let frame = createFrame "X"
    Assert.Equal(10, calculateSpareScore frame)
    
[<Fact>]
let ``calculateTotalScore correctly counts score of just ints`` () =
    let frames = "11 22 33 44 54 63 72 81 11 22" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(62, calculateTotalScore frames 0 0)

[<Fact>]
let ``calculateTotalScore correctly counts score with spares involved`` () =
    let frames = "11 22 33 44 5/ 63 72 8/ 11 22" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(71, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score with strikes involved`` () =
    let frames = "X 22 33 44 X X 72 81 11 22" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(102, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score with a mixture of all throw types involved`` () =
    let frames = "X 2/ 33 44 X 5/ 72 81 11 22" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(108, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score when bonus throws are involved`` () =
    let frames = "X X X X X X X X X X X X" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(300, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score when bonus throw is spare`` () =
    let frames = "X X X X X X X X X X 7/" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(287, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score when bonus throw is numbers`` () =
    let frames = "X X X X X X X X X X 43" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(281, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score when all frames have a miss`` () =
    let frames = "9- 9- 9- 9- 9- 9- 9- 9- 9- 9-" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(90, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateTotalScore correctly counts score when all frames are spare with a final 5`` () =
    let frames = "5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5-" |> splitLine |> List.map(createFrame)
    
    Assert.Equal(150, calculateTotalScore frames 0 0)
    
[<Fact>]
let ``calculateBonusScore correctly calculates bonus score when final throw is strike`` () =
    let frame = createFrame "7/"
    Assert.Equal(10, calculateBonusScore true frame)
    
[<Fact>]
let ``calculateBonusScore correctly calculates bonus score when final throw is spare`` () =
    let frame = createFrame "7-"
    Assert.Equal(7, calculateBonusScore false frame)
    
